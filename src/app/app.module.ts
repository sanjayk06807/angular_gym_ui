import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeadinParagraphComponent } from './headin-paragraph/headin-paragraph.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadinParagraphComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
