import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadinParagraphComponent } from './headin-paragraph.component';

describe('HeadinParagraphComponent', () => {
  let component: HeadinParagraphComponent;
  let fixture: ComponentFixture<HeadinParagraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeadinParagraphComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeadinParagraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
